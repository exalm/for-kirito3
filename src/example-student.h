#pragma once

#include <glib-object.h>

G_BEGIN_DECLS

#define EXAMPLE_TYPE_STUDENT (example_student_get_type())

G_DECLARE_FINAL_TYPE (ExampleStudent, example_student, EXAMPLE, STUDENT, GObject)

ExampleStudent *example_student_new (gint roll_no);

gint            example_student_get_roll_no (ExampleStudent *self);
void            example_student_set_roll_no (ExampleStudent *self,
                                             gint            roll_no);

G_END_DECLS
